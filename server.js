
var express  = require('express');
var app      = express();
var port     = process.env.PORT || 8080;
var mongoose = require('mongoose');
var logger = require('morgan');  
var path = require('path');  
var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var boards = require('./routes/boards');
mongoose.connect('mongodb://127.0.0.1:27017/mydb');
app.use(bodyParser()); 
app.set('view engine', 'ejs'); 
app.use(logger('dev'));  
app.use(bodyParser.json());  
app.use(bodyParser.urlencoded({ extended: false }));  
app.use(cookieParser());  
app.use(express.static(path.join(__dirname, 'public')));
app.use('/boards', boards);
app.use(function(req, res, next) {  
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });
  if (app.get('env') === 'development') {  
    app.use(function(err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err,
      });
    });
  }
  app.use(function(err, req, res, next) {  
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: {},
    });
  });
app.listen(port);
console.log('server is now running on port ' + port);

