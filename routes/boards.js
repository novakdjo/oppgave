/* author: Quoc */
var express = require('express');
var router = express.Router();
var passport = require('passport');
var back = require('express-back');
var session = require('express-session');
router.use(session({secret:'XASDASDA'}));
var ssn ;
var database = require('../database/databaseoperation.js');
router.get('/',  function(req, res, next) {
  database.boards().then(
    function (items) {
      console.log(items);
      res.render('boards', { "boards": items });
    }
    );
});
router.get('/newboard', function (req, res) {
  res.render('newboard', { title: 'Add new board' });
});
router.post('/addboard', function (req, res) { 
  var board = {
    "boardname": req.body.boardname,
    "boardv": req.body.v2,
  };
    database.addboard(board)
    .then(
      function (result) { console.log(result); res.redirect("/boards"); },
      function (err) {
        console.error('problem .........:', err);
        res.send("problem with the database. " + err);
      }
      );
});
router.get('/:key', function (req, res) {
  var boards = req.params.key;
  database.getsboard(boards)
  .then(
    function (items) {
      console.log(items);
      res.render('boarddata', { "boards": items[0] });
    }
    );
});
router.get('/:key/newnotes', function (req, res) {
  var boardkey = req.params.key;
  database.getcurrentboard(boardkey)
    .then(function (items) {
      console.log(items);
      res.render('addnotes', { "boards": items[0] });
    },
      function (err) {
        console.error('problem ......', err);
        res.send("problem with the database. " + err);
      }
      );
});
router.post('/:key/addnotes', function (req, res) {
  var n_notes = {
    "boardkey": req.params.key,
    "notename": req.body.notename,
    "notes": req.body.notes,
    //"noteswritename": req.user.fullname,
  };
   database.addnotes(n_notes)
      .then(
        function (result) { console.log(result); res.redirect("/boards"); },
        function (err) {
          console.error('problem ......', err);
          res.send("problem with the database. " + err);
        }
        );
});
router.get('/:key/getnotes', function (req, res) {
  var boardkey = req.params.key;
  var bownowner = {
    "boardownname": req.body.funame,
    "boardkey": req.params.key,
  };
  database.getnotes(boardkey)
    .then(function (items) {
      console.log(items);
      res.render('boardnotes', { "notes": items});
    },
      function (err) {
        console.error('Problem ....:', err);
        res.send("problem with database. " + err);
      }
      );
});
router.get('/:key/notesinfo/:key', function (req, res) {
  ssn = req.session;
  ssn.current = req.header('Referer');
  var notekey = req.params.key;
  database.getcurrentnotes(notekey) 
    .then(function (items) {
      console.log(items);
      res.render('notesdata', { "notes": items[0] , user: req.user});
    },
      function (err) {
        console.error('problem .......', err);
        res.send("problem with the database. " + err);
      }
      );
});
router.get('/:key/notesinfo/:key/update', function (req, res) {
  var boardkey = req.params.key;
  database.getcurrentnotes(boardkey) 
    .then(function (list) {
      console.log(list);
      res.render('changenotes', { "notes": list[0] });
    },
      function (err) {
        console.error('Problem ......:', err);
        res.send("problem with the database. " + err);
      }
      );
});
router.post('/:key/notesinfo/:key/update', function (req, res) {
  req.session.current_url = '/:key/notesinfo/:key/update';
  var preurl = req.session;
  if(preurl ==  ssn.current) var preurlv = preurl;
  var info = req.params.key ;
  var noteinfo = {
    "key": req.params.key,
    "notename": req.body.notename,
    "notes": req.body.notes
  };
  database.changenotes(noteinfo)
    .then(
      function (result) { console.log(result); res.redirect(  ssn.current); },
      function (err) {
        console.error('Problem .....:', err);
        res.send("problem with the database. " + err);
      }
      );
});
router.get('/:key/newboard', function (req, res) {
  res.render('addboard', { title: 'Add new board' });
});
router.get('/:key/allowusers', function (req, res) {
  database.userslist().then(
    function (items) {
      res.render('isallowusers', { "allusers": items , user: req.user.username });
    },
    function (err) {
      console.error('Problem ......', err);
      res.send("Problem processing to the database. " + err);
    }
    );
});
router.get('/:key/updateboard', function (req, res) {
  var boardkey = req.params.key;
  database.getboarddbykey(boardkey) 
    .then(function (list) {
      database.userslist().then(
        function (items) {
         console.log(list);
      res.render('updateboard', { "data": list[0] , "allusers": items  });
    }  )
 },
      function (err) {
        console.error('Problem ......:', err);
        res.send("problem with the database. " + err);
      }
      );
});
router.post('/:key/updateboard', function (req, res) {
  var noteinfo = {
    "key": req.params.key,
    "boardname": req.body.boardname,
    "boardvalues": req.body.v2
  };
  database.changeboarddata(noteinfo)
    .then(
      function (result) { console.log(result); res.redirect("/boards"); },
      function (err) {
        console.error('Problem .....:', err);
        res.send("problem with the database. " + err);
      }
      );
});
router.get('/:key/addnotes', function (req, res) {
  res.render('addnotes', { title: 'Add new notes' });
});
router.get('/:key/getnotesread', function (req, res) {
  var boardkey = req.params.key;
  var bown = {
    "boardown": req.body.funame,
  };
  database.getnotesbyboardkey(boardkey)
    .then(function (items) {
      console.log(items);
      res.render('boardnotesread', { "notes": items , "boown" : boardkey });
    },
      function (err) {
        console.error('Problem ....:', err);
        res.send("problem with database. " + err);
      }
      );
});
router.get('/:key/getnotes', function (req, res) {
  var boardkey = req.params.key;
  var bownowner = {
    "boardownname": req.body.funame,
    "boardkey": req.params.key,
  };
  database.getnotesbyboardkey(boardkey)
    .then(function (items) {
      console.log(items);
      res.render('boardnotes', { "notes": items , "boown" : boardkey });
    },
      function (err) {
        console.error('Problem ....:', err);
        res.send("problem with database. " + err);
      }
      );
});
router.get('/:key/notesinfo/:key/update', function (req, res) {
  var boardkey = req.params._id;
  database.getnotesbykey(boardkey) 
    .then(function (list) {
      console.log(list);
      res.render('changenotes', { "notes": list[0] });
    },
      function (err) {
        console.error('Problem ......:', err);
        res.send("problem with the database. " + err);
      }
      );
});
router.post('/:key/notesinfo/:key/update', function (req, res) {
  var noteinfo = {
    "key": req.params.key,
    "notename": req.body.notename,
    "notes": req.body.notes
  };
  database.changenotes(noteinfo)
    .then(
      function (result) { console.log(result); res.redirect("/boards"); },
      function (err) {
        console.error('Problem .....:', err);
        res.send("problem with the database. " + err);
      }
      );
});
router.get('/:key/notesinfo/:key/delete', function (req, res) {
  var userkey = req.params.key;
  database.deletenotes(userkey)
    .then(function (list) {
      console.log(list);
      res.redirect("/boards");
    },
      function (err) {
        console.error('problem ......:', err);
        res.send("problem with the database. " + err);
      }
      );
});
router.post('/:key/addboard', function (req, res) { 
  // var user = {
  //   "username": req.body.username,
  //   "email": req.body.useremail
  // };
  var board = {
    "userkey": req.params.key,
    "boardname": req.body.boardname,
    "boardv": req.body.v2,
    //"boardv": req.body.xx    ? true : false,
    "boardownroles": req.user.role
  };
    database.addboard2(board)
    .then(
      function (result) { console.log(result); res.redirect("/boards"); },
      function (err) {
        console.error('problem .........:', err);
        res.send("problem with the database. " + err);
      }
      );
});
router.get('/:key/newnotes', function (req, res) {
  var boardkey = req.params.key;
  database.getboardbykey(boardkey)
    .then(function (items) {
      console.log(items);
      res.render('addnotes', { "boards": items[0] });
    },
      function (err) {
        console.error('problem ......', err);
        res.send("problem with the database. " + err);
      }
      );
});
router.post('/:key/addnotes', function (req, res) {
  var n_notes = {
    "boardkey": req.params.key,
    "notename": req.body.notename,
    "notes": req.body.notes,
    "noteswritename": req.user.fullname,
  };
   database.addnotes(n_notes)
      .then(
        function (result) { console.log(result); res.redirect("/boards"); },
        function (err) {
          console.error('problem ......', err);
          res.send("problem with the database. " + err);
        }
        );

});
module.exports = router;

