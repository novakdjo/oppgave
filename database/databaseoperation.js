/* author: Quoc */
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";
module.exports = {
  boards : function()
  {        
     return MongoClient.connect(url).then(function (db) {          
      return db.collection('boards').find({}, {'boardvalues': true, 'boardname': true}).toArray();
      }).then(function(items) {
      console.log(items);
      return items;
      db.close(); 
      });
    },
    addboard : function(boards)
    {
        return  MongoClient.connect(url).then(function (db) {
          var ObjectID = require('mongodb').ObjectID;     
          var myobj = {'boardname': boards.boardname, 'boardvalues': boards.boardv};
          db.collection("boards").insertOne(myobj, function(err, res) {
            if (err) throw err;
            console.log("document inserted");
            db.close();               
          });
        });
      },
      getsboard : function(boards)
      {
          return MongoClient.connect(url).then(function (db) {
          var ObjectID = require('mongodb').ObjectID;
          var o_id = new ObjectID(boards);  
          return db.collection('boards').find({'_id': o_id }, {'boardname': true,'boardvalues': true }).toArray(); 
          }).then(function(items) {
          console.log(items);
          return items;
          db.close(); 
          });            
      },
      addnotes : function(boards)
      {
          return  MongoClient.connect(url).then(function (db) {
            var ObjectID = require('mongodb').ObjectID;
            //var o_id = new ObjectID(user);
            //var ObjectID = require('mongodb').ObjectID;
            var o_id = new ObjectID(boards.boardkey);
            var myobj = {'boardskey': o_id, 'notename': boards.notename,
            'notes': boards.notes };
            db.collection("notes").insertOne(myobj, function(err, res) {
              if (err) throw err;
              console.log("document inserted");
              db.close();               
            });
          });
        },
       getnotes: function(notes)
       {
       return MongoClient.connect(url).then(function (db) {
        var ObjectID = require('mongodb').ObjectID;
        var o_id = new ObjectID(notes);
        return db.collection('notes').find({'boardskey': o_id }, {'notename': true, 'notes': true}).toArray();  
        }).then(function(items) {
        console.log(items);
        return items;
        });
      },
      getcurrentboard : function(board)
      {
         return MongoClient.connect(url).then(function (db) {
          var ObjectID = require('mongodb').ObjectID;
          var o_id = new ObjectID(board);        
          return db.collection('boards').find({}, {'_id': o_id } ).toArray();          
          }).then(function(items) {
          console.log(items);
          return items;
          db.close(); 
          });      
      },
      getcurrentnotes : function(notes)
      {          
      return MongoClient.connect(url).then(function (db) {
        var ObjectID = require('mongodb').ObjectID;
        var o_id = new ObjectID(notes);  
        return db.collection('notes').find({'_id': o_id }, {'notename': true, 'notes': true, 'boardkey': true}).toArray();
        }).then(function(items) {
        console.log(items);
        return items;
        });
      },
      deletenotes : function(key)
      {
        return  MongoClient.connect(url).then(function (db) {
          var ObjectID = require('mongodb').ObjectID;
          var o_id = new ObjectID(key);
          db.collection("notes").remove({_id: o_id}, function(err, res) {
            if (err) throw err;
            console.log("document deleted");
            db.close();               
          });
        });
      },
  getallowedusers : function(user)
  {
    return MongoClient.connect(url).then(function (db) {
     var ObjectID = require('mongodb').ObjectID;
     var o_id = new ObjectID(user);
     return db.collection('readaccessusers').find({'_id': o_id }, {'readaccessuser': true }).toArray();  
     }).then(function(items) {
     console.log(items);
     return items;
     db.close(); 
     });      
 },
  getboardvaluesbykey : function(user)
  {
     return MongoClient.connect(url).then(function (db) {
      var ObjectID = require('mongodb').ObjectID;
      var o_id = new ObjectID(user);
      return db.collection('boards').find({'_id': o_id },{'boardvalues': true}).toArray();  
      }).then(function(items) {
      console.log(items);
      return items;
      db.close(); 
      });      
  },
  getboarddbykey : function(user)
  {
     return MongoClient.connect(url).then(function (db) {
      var ObjectID = require('mongodb').ObjectID;
      var o_id = new ObjectID(user);
      return db.collection('boards').find({'_id': o_id }).toArray();  
      }).then(function(items) {
      console.log(items);
      return items;
      db.close(); 
      });      
  },
  changenotes : function(noteinfo)
  {  
     return MongoClient.connect(url).then(function (db) {   
      var ObjectID = require('mongodb').ObjectID;
      var o_id = new ObjectID(noteinfo.key);
      var myquery = { _id: o_id } ;
      var newvalues = { $set: {'notename': noteinfo.notename,'notes': noteinfo.notes}};
      db.collection('notes').update(myquery , newvalues
      , function(err, res) {             
        if (err) throw err;
        console.log("document is updated");
        db.close();     
      });
     });    
  },
  addboardold : function(boards)
  {
      return  MongoClient.connect(url).then(function (db) {
        var ObjectID = require('mongodb').ObjectID;
        //var o_id = new ObjectID(user);
        //var ObjectID = require('mongodb').ObjectID;
        var o_id = new ObjectID(boards.userkey);
        var myobj = {'boardname': boards.boardname, 'boardvalues': boards.boardv,
        'user_id': o_id};
        db.collection("boards").insertOne(myobj, function(err, res) {
          if (err) throw err;
          console.log("document inserted");
          db.close();               
        });
      });
    },
    addboard2 : function(boards)
    {
      return MongoClient.connect(url).then(function (db) {
        var ObjectID = require('mongodb').ObjectID;
        var o_id = new ObjectID(boards.userkey);
        return db.collection('users').find( {'_id': o_id } , {'fullname': true } ).toArray();  
        }).then(function(items) {   
         MongoClient.connect(url).then(  function(db) {     
          var myobj = {'boardname': boards.boardname, 'boardvalues': boards.boardv, 
          'ownroles': boards.boardownroles,
          'username': items};
           return  db.collection("boards").insertOne(myobj, function(err, res) {
            if (err) throw err;
            console.log("document inserted");
            db.close();               
          }); 
      })});
    },
       getnotesbyboardkey: function(boardkey)
       {
       return MongoClient.connect(url).then(function (db) {
        var ObjectID = require('mongodb').ObjectID;
        var o_id = new ObjectID(boardkey);
        return db.collection('notes').find({'boardkey': o_id },{'notename': true}, {'notes' : true}).toArray();  
        }).then(function(items) {
        console.log(items);
        return items;
        });
      },
        changeboarddata : function(info)
        {  
           return MongoClient.connect(url).then(function (db) {   
            var ObjectID = require('mongodb').ObjectID;
            var o_id = new ObjectID(info.key);
             //var myquery = { _id: o_id ,  'boardname': true, 'boardvalues': true } ;
             var myquery = { _id: o_id  } ;
             var newvalues = { $set: { 'boardname': info.boardname, 'boardvalues': info.boardvalues}} ;
            db.collection('boards').update(myquery , newvalues 
            , function(err, res) {             
              if (err) throw err;
              console.log("document is updated");
              db.close();     
            });
           });    
        },      
    } 

    
  